package az.izobretatel7777.hackersuitcase.controller;

import az.izobretatel7777.hackersuitcase.dao.entity.User;
import az.izobretatel7777.hackersuitcase.service.ProfileService;
import az.izobretatel7777.hackersuitcase.service.UpdatePasswordService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

@ExtendWith(MockitoExtension.class)
public class LoginControllerTest {

    private MockMvc mockMvc;

    @Mock
    private UpdatePasswordService updatePasswordService;
    @Mock
    private ProfileService profileService;

    @InjectMocks
    private LoginController loginController;

    @BeforeEach
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(loginController)
                    .setViewResolvers(viewResolver())
                    .build();
    }

    private ViewResolver viewResolver() {
        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setPrefix("/WEB-INF/views/"); // Set this to match your view directory
        viewResolver.setSuffix(".html"); // Set this to match your view file extension
        return viewResolver;
    }


    @Test
    public void testLogin() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"));
    }

    @Test
    public void testResetPassword() throws Exception {
        mockMvc.perform(post("/reset_password")
                        .param("newPassword", "newPass123")
                        .param("otp", "123456"))
                .andExpect(status().isOk())
                .andExpect(view().name("register_finished"));
        verify(updatePasswordService).resetPassword("newPass123", "123456");
    }

    @Test
    public void testForgotPasswordView() throws Exception {
        mockMvc.perform(get("/forget_password"))
                .andExpect(status().isOk())
                .andExpect(view().name("forget_password"));
    }

    @Test
    public void testForgotPassword() throws Exception {
        mockMvc.perform(post("/forget_password")
                        .param("email", "test@example.com"))
                .andExpect(status().isOk())
                .andExpect(view().name("reset_password_form"));
        verify(updatePasswordService).sendResetPasswordEmail("test@example.com");
    }

    @Test
    public void testProfile() throws Exception {
        mockMvc.perform(get("/profile"))
                .andExpect(status().isOk())
                .andExpect(view().name("profile"));
        verify(profileService).getProfile();
    }

    @Test
    public void testUpdateProfile() throws Exception {
        User testUser = new User(); // Assume User is a valid entity class
        mockMvc.perform(post("/profile")
                        .flashAttr("user", testUser))
                .andExpect(status().isOk())
                .andExpect(view().name("profile"));
        verify(profileService).updateProfile(testUser);
    }

    @Test
    public void testLogout() throws Exception {
        mockMvc.perform(get("/logout"))
                .andExpect(status().isOk())
                .andExpect(view().name("logout"));
    }
}