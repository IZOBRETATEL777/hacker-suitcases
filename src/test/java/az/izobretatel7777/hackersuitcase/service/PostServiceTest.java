package az.izobretatel7777.hackersuitcase.service;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import az.izobretatel7777.hackersuitcase.dao.entity.Post;
import az.izobretatel7777.hackersuitcase.dao.entity.User;
import az.izobretatel7777.hackersuitcase.dao.repo.PostRepository;
import az.izobretatel7777.hackersuitcase.dao.repo.UserRepository;
import az.izobretatel7777.hackersuitcase.service.implementations.PostServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.Timestamp;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class PostServiceTest {

    @Mock
    private PostRepository postRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Authentication authentication;

    @InjectMocks
    private PostServiceImpl postService;

    @Test
    public void testGetAllPosts() {
        Post post1 = new Post(/* set properties */);
        Post post2 = new Post(/* set properties */);
        List<Post> posts = Arrays.asList(post1, post2);

        when(postRepository.findAll()).thenReturn(posts);

        List<Post> retrievedPosts = postService.getAllPosts();

        assertEquals(posts, retrievedPosts);
        verify(postRepository).findAll();
    }

    @Test
    public void testDeletePostById() {
        long postId = 1L;
        User mockUser = new User();
        mockUser.setEmail("user@example.com");
        Post mockPost = new Post();
        mockPost.setAuthor(mockUser);

        when(postRepository.getById(postId)).thenReturn(mockPost);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("user@example.com");
        SecurityContextHolder.setContext(securityContext);

        assertTrue(postService.deletePostById(postId));
        verify(postRepository).deleteById(postId);
    }

    @Test
    public void testGetPostById() {
        long postId = 1L;
        Post mockPost = new Post(/* set properties */);

        when(postRepository.findById(postId)).thenReturn(Optional.of(mockPost));

        Post retrievedPost = postService.getPostById(postId);

        assertEquals(mockPost, retrievedPost);
        verify(postRepository).findById(postId);
    }

    @Test
    public void testSavePost() {
        String title = "Test Title";
        String content = "Test Content";
        User mockUser = new User();
        mockUser.setEmail("user@example.com");

        when(securityContext.getAuthentication()).thenReturn(authentication);
        when(authentication.getName()).thenReturn("user@example.com");
        when(userRepository.findByEmail("user@example.com")).thenReturn(mockUser);
        SecurityContextHolder.setContext(securityContext);

        postService.savePost(title, content);

        verify(postRepository, times(2)).save(any(Post.class));
    }
}
