package az.izobretatel7777.hackersuitcase.service;

import az.izobretatel7777.hackersuitcase.dao.entity.User;
import az.izobretatel7777.hackersuitcase.dao.repo.UserRepository;
import az.izobretatel7777.hackersuitcase.service.implementations.RegistrationServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class RegistrationServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private EmailingService emailingService;
    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    @InjectMocks
    private RegistrationServiceImpl registrationService;

    private final String fromEmail = "noreply@hackersuitcases.com"; // Mock the value that would be injected

    @BeforeEach
    public void setUp() {
        // Use ReflectionTestUtils to inject 'fromEmail' field value if necessary
        ReflectionTestUtils.setField(registrationService, "fromEmail", fromEmail);
    }

    @Test
    public void testRegister() {
        User user = new User();
        user.setEmail("test@example.com");
        user.setPassword("password");
        // Assume OTP generation is handled within the service and can be tested indirectly
        when(passwordEncoder.encode(anyString())).thenReturn("encodedPassword");

        registrationService.register(user);

        verify(userRepository).save(ArgumentMatchers.any(User.class));
        // Adjust this to match the exact email content if necessary
        verify(emailingService).sendEmail(anyString(), eq("test@example.com"), contains("Welcome to HackerSuitcases forum!"));
    }

    @Test
    public void testActivateUserByOtp() {
        User user = new User();
        user.setOtp("123456");

        when(userRepository.findByOtp("123456")).thenReturn(user);

        boolean result = registrationService.activateUserByOtp("123456");

        assertTrue(result);
        verify(userRepository).save(user);
    }

    @Test
    public void testActivateUserByOtpWhenUserNotFound() {
        when(userRepository.findByOtp("invalidOtp")).thenReturn(null);

        boolean result = registrationService.activateUserByOtp("invalidOtp");

        assertFalse(result);
        verify(userRepository, never()).save(ArgumentMatchers.any(User.class));

    }
}
