package az.izobretatel7777.hackersuitcase.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import az.izobretatel7777.hackersuitcase.dao.entity.Comment;
import az.izobretatel7777.hackersuitcase.dao.entity.CommentVote;
import az.izobretatel7777.hackersuitcase.dao.entity.Post;
import az.izobretatel7777.hackersuitcase.dao.entity.User;
import az.izobretatel7777.hackersuitcase.dao.repo.CommentRepository;
import az.izobretatel7777.hackersuitcase.dao.repo.CommentVoteRepository;
import az.izobretatel7777.hackersuitcase.dao.repo.PostRepository;
import az.izobretatel7777.hackersuitcase.dao.repo.UserRepository;
import az.izobretatel7777.hackersuitcase.service.implementations.CommentServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Optional;

@ExtendWith(MockitoExtension.class)
public class CommentServiceTest {

    @Mock
    private CommentRepository commentRepository;
    @Mock
    private UserRepository userRepository;
    @Mock
    private CommentVoteRepository commentVoteRepository;
    @Mock
    private PostRepository postRepository;
    @Mock
    private Authentication authentication;

    @InjectMocks
    private CommentServiceImpl commentService;

    @BeforeEach
    public void setup() {
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

    @Test
    public void testDeleteComment() {
        long commentId = 1L;
        User mockUser = new User(); // Create a mock User
        mockUser.setEmail("test@example.com");
        Comment mockComment = new Comment();
        mockComment.setAuthor(mockUser);

        when(authentication.getName()).thenReturn("test@example.com");
        when(commentRepository.getById(commentId)).thenReturn(mockComment);

        boolean result = commentService.deleteComment(commentId);

        assertTrue(result);
        verify(commentRepository).delete(mockComment);
    }

    @Test
    public void testLikeComment() {
        long commentId = 1L;
        User mockUser = new User();
        mockUser.setEmail("user@example.com");
        mockUser.setId(2L);
        Comment mockComment = new Comment();
        mockComment.setId(commentId);
        mockComment.setRating(0L);

        when(authentication.getName()).thenReturn("user@example.com");
        when(commentRepository.getById(commentId)).thenReturn(mockComment);
        when(userRepository.findByEmail("user@example.com")).thenReturn(mockUser);
        when(commentVoteRepository.findByUser_IdAndComment_Id(2L, commentId)).thenReturn(null);

        commentService.likeComment(commentId);

        verify(commentRepository).save(any(Comment.class));
        verify(commentVoteRepository).save(any(CommentVote.class));
    }

    @Test
    public void testDislikeComment() {
        long commentId = 1L;
        User mockUser = new User();
        mockUser.setEmail("user@example.com");
        mockUser.setId(2L);
        Comment mockComment = new Comment();
        mockComment.setId(commentId);
        mockComment.setRating(0L);

        when(authentication.getName()).thenReturn("user@example.com");
        when(commentRepository.getById(commentId)).thenReturn(mockComment);
        when(userRepository.findByEmail("user@example.com")).thenReturn(mockUser);
        when(commentVoteRepository.findByUser_IdAndComment_Id(2L, commentId)).thenReturn(null);

        commentService.dislikeComment(commentId);

        verify(commentRepository).save(any(Comment.class));
        verify(commentVoteRepository).save(any(CommentVote.class));
    }

    @Test
    public void testAddComment() {
        long postId = 1L;
        String content = "Test comment";
        User mockUser = new User();
        mockUser.setEmail("user@example.com");

        when(authentication.getName()).thenReturn("user@example.com");
        when(userRepository.findByEmail("user@example.com")).thenReturn(mockUser);
        when(postRepository.findById(postId)).thenReturn(Optional.of(new Post()));

        commentService.addComment(postId, content);

        verify(commentRepository).save(any(Comment.class));
    }
}
