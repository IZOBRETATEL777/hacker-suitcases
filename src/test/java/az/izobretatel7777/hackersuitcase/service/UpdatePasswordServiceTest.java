package az.izobretatel7777.hackersuitcase.service;

import az.izobretatel7777.hackersuitcase.dao.entity.User;
import az.izobretatel7777.hackersuitcase.dao.repo.UserRepository;
import az.izobretatel7777.hackersuitcase.service.implementations.EmailingServiceImpl;
import az.izobretatel7777.hackersuitcase.service.implementations.UpdatePasswordServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UpdatePasswordServiceTest {

    @Mock
    private UserRepository userRepository;
    @Mock
    private EmailingServiceImpl emailingService;
    @Mock
    private BCryptPasswordEncoder passwordEncoder;
    @Mock
    private SecurityContext securityContext;
    @Mock
    private Authentication authentication;

    @InjectMocks
    private UpdatePasswordServiceImpl updatePasswordService;

    private final String fromEmail = "noreply@hackersuitcases.com";

    @BeforeEach
    public void setUp() {
        ReflectionTestUtils.setField(updatePasswordService, "fromEmail", fromEmail);
        Mockito.lenient().when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Test
    public void testSendResetPasswordEmail() {
        String email = "test@example.com";
        User mockUser = new User();
        mockUser.setEmail(email);
        mockUser.setOtp("123456");

        when(userRepository.findByEmail(email)).thenReturn(mockUser);

        updatePasswordService.sendResetPasswordEmail(email);

        // Adjust this string to match the actual content used in the service
        String expectedEmailContent = "Hello from HackerSuitcase forum! Use this code to change your password:\n123456";
        verify(userRepository).findByEmail(email);
        verify(emailingService).sendEmail(fromEmail, email, expectedEmailContent);
    }


    @Test
    public void testResetPassword() {
        String newPassword = "newPassword";
        String otp = "123456";
        User mockUser = new User();
        mockUser.setOtp(otp);

        when(userRepository.findByOtp(otp)).thenReturn(mockUser);
        when(passwordEncoder.encode(newPassword)).thenReturn("encodedPassword");

        boolean result = updatePasswordService.resetPassword(newPassword, otp);

        assertTrue(result);
        verify(userRepository).findByOtp(otp);
        verify(passwordEncoder).encode(newPassword);
        verify(userRepository).save(any(User.class));
    }

    @Test
    public void testSendResetPasswordEmailCurrentUser() {
        String email = "user@example.com";
        User mockUser = new User();
        mockUser.setEmail(email);
        mockUser.setOtp("123456");

        when(authentication.getName()).thenReturn(email);
        when(userRepository.findByEmail(email)).thenReturn(mockUser);

        updatePasswordService.sendResetPasswordEmail();

        // Match the actual email content
        String expectedEmailContent = "Hello from HackerSuitcase forum! Use this code to change your password:\n123456";
        verify(userRepository).findByEmail(email);
        verify(emailingService).sendEmail(fromEmail, email, expectedEmailContent);
    }

}
