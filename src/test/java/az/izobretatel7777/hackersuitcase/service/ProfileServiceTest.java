package az.izobretatel7777.hackersuitcase.service;

import az.izobretatel7777.hackersuitcase.dao.entity.User;
import az.izobretatel7777.hackersuitcase.dao.repo.UserRepository;
import az.izobretatel7777.hackersuitcase.service.implementations.ProfileServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ProfileServiceTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private SecurityContext securityContext;

    @Mock
    private Authentication authentication;

    @InjectMocks
    private ProfileServiceImpl profileService;

    @BeforeEach
    public void setUp() {
        SecurityContextHolder.setContext(securityContext);
        when(securityContext.getAuthentication()).thenReturn(authentication);
    }

    @Test
    public void testUpdateProfile() {
        User user = new User();
        user.setEmail("test@example.com");
        when(authentication.getName()).thenReturn("test@example.com");
        when(userRepository.findByEmail("test@example.com")).thenReturn(user);

        boolean result = profileService.updateProfile(user);

        assertTrue(result);
        verify(userRepository).save(ArgumentMatchers.any(User.class));
    }

//    @Test
//    public void testUpdateProfileWhenUserNotFound() {
//        User user = new User();
//        user.setEmail("nonexistent@example.com");
//        when(authentication.getName()).thenReturn("nonexistent@example.com");
//        when(userRepository.findByEmail("nonexistent@example.com")).thenReturn(null);
//
//        boolean result = profileService.updateProfile(user);
//
//        assertFalse(result);
//        verify(userRepository, never()).save(ArgumentMatchers.any(User.class));
//    }

    @Test
    public void testGetProfile() {
        User user = new User();
        user.setEmail("test@example.com");
        when(authentication.getName()).thenReturn("test@example.com");
        when(userRepository.findByEmail("test@example.com")).thenReturn(user);

        User result = profileService.getProfile();

        assertNotNull(result);
        assertNull(result.getPassword());
    }

//    @Test
//    public void testGetProfileWhenUserNotFound() {
//        when(authentication.getName()).thenReturn("nonexistent@example.com");
//        when(userRepository.findByEmail("nonexistent@example.com")).thenReturn(null);
//
//        User result = profileService.getProfile();
//
//        assertNull(result);
//    }
}
