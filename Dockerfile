FROM eclipse-temurin:17

WORKDIR .

COPY build/libs/*SNAPSHOT.jar app.jar

ENTRYPOINT ["java", "-jar", "app.jar"]

